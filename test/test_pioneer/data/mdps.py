from pioneer.mdp.known import KnownMDP, Dynamics
from pioneer.utils.dist import Discrete


def deterministic_line_mdp(size):
    states = list(range(size))

    state = {}
    reward = {}
    for s in states:
        state[(s, 1)] = Discrete.deterministic(s + 1)
        reward[(s, 1)] = Discrete.deterministic(1)
    dynamics = Dynamics(state, reward)
    terminal = lambda s: s == size - 1
    return KnownMDP(dynamics, terminal)
