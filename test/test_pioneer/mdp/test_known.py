import pytest
from pioneer.utils.dist import Discrete
from test_pioneer.data import deterministic_line_mdp


class TestKnownMDP:
    @pytest.mark.parametrize("size", range(1, 100, 20))
    def test_sequence_length_deterministic_dynamics(self, size):
        mdp = deterministic_line_mdp(size)
        policy = {s: Discrete.deterministic(1) for s in mdp.states}
        episode = list(mdp.episode(0, policy))
        assert len(episode) == size - 1
