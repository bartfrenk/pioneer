from copy import deepcopy
from pioneer.algorithm import monte_carlo_es
from pioneer.utils.dist import Discrete
from test_pioneer.data import deterministic_line_mdp


class TestMonteCarloEs:
    def test_monte_carlo_es_on_deterministic_line(self):
        mdp = deterministic_line_mdp(10)
        initial = {s: Discrete.deterministic(1) for s in mdp.states}
        policy = deepcopy(initial)
        it = monte_carlo_es(mdp, policy, 1)
        for _ in range(10):
            next(it)
            assert initial == policy
