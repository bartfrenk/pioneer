from pioneer.examples import GridWorld, Action
from pioneer.policies import create_uniform_policy


class TestGridWorld:
    def test_random_policy_in_small_grid_world_ends_in_terminal_state(self):
        mdp = GridWorld(10, 10)
        policy = create_uniform_policy(mdp.states, list(Action))
        episode = list(mdp.episode((1, 1), policy))
        assert mdp.terminal(episode[-1].end)
