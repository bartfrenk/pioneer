import random
from typing import Dict, Tuple, List, Optional, Sequence, Iterator

from pioneer.base import A, R, S
from pioneer.mdp.base import MDP, Step
from pioneer.policies.base import MutablePolicy
from pioneer.utils.dist import Discrete


def monte_carlo_es(
    mdp: MDP[S, A, R], policy: MutablePolicy[S, A], discount: R
) -> Iterator[None]:
    state = random.choice(mdp.states)
    action = random.choice(mdp.actions(state))
    returns: Dict[Tuple[S, A], List[R]]

    cumulative: R = 0  # type: ignore
    while True:
        episode = list(mdp.episode(state, policy, action))
        for t in range(len(episode) - 1, 0):
            reward = episode[t + 1].reward
            if reward is None:
                break
            cumulative = discount * cumulative + reward  # type: ignore

            if not visited(episode[:t], episode[t].start, episode[t].action):
                returns.setdefault((state, action), []).append(cumulative)
            optimal = greedy(returns, mdp.actions(episode[t].start), episode[t].start)
            if optimal is None:
                raise RuntimeError("Failed to compute greedy action")
            policy[state] = Discrete.deterministic(optimal)
        yield


def visited(episode: Sequence[Step[S, A, R]], state: S, action: A) -> bool:
    for step in episode:
        if step.start == state and step.action == action:
            return True
    return False


def greedy(returns: Dict[Tuple[S, A], List[R]], actions: Sequence[A], state: S) -> Optional[A]:
    action = None
    value = None
    for a in actions:
        if (state, a) in returns:
            if value is None or returns[(state, a)] > value:
                action = a
                value = returns[(state, a)]
    return action
