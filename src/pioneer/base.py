from decimal import Decimal
from typing import TypeVar, Callable


X = TypeVar("X")

S = TypeVar("S")
A = TypeVar("A")
R = TypeVar("R", float, Decimal)

Pred = Callable[[X], bool]
