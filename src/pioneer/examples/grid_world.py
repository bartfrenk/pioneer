from itertools import product
from enum import Enum, auto
from typing import Sequence, Tuple, Optional, Iterator
from pioneer.mdp import MDP, Step
from pioneer.policies import Policy


class Action(Enum):
    LEFT = auto()
    UP = auto()
    RIGHT = auto()
    DOWN = auto()


State = Tuple[int, int]


def _bound(x: int, lo: int, hi: int) -> int:
    if x < lo:
        return lo
    if x >= hi:
        return hi - 1
    return x


class GridWorld(MDP[State, Action, float]):
    _REWARD = -1

    def __init__(self, rows: int, columns: int):
        self._rows = rows
        self._columns = columns

    def iterate(
        self, state: State, policy: Policy[State, Action], action: Optional[Action] = None
    ) -> Iterator[Step[State, Action, float]]:
        start = state
        action_: Action = action or policy[start].draw()  # type: ignore
        while True:
            end = self._transition(start, action_)
            yield Step(start, end, action_, self._REWARD)
            start = end
            action_ = policy[start].draw()

    @property
    def states(self) -> Sequence[State]:
        return list(product(range(self._rows), range(self._columns)))

    def actions(self, state: State) -> Sequence[Action]:
        return list(Action)

    def terminal(self, state: State) -> Optional[bool]:
        return state in [(0, 0), (self._rows - 1, self._columns - 1)]

    def _transition(self, state: State, action: Action) -> State:
        (r, c) = state
        if action == Action.LEFT:
            return (r, max(c - 1, 0))
        if action == Action.UP:
            return (max(r - 1, 0), c)
        if action == Action.RIGHT:
            return (r, min(c + 1, self._columns - 1))
        if action == Action.DOWN:
            return (min(r + 1, self._rows - 1), c)
        raise ValueError("Invalid action")
