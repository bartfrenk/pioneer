from typing import Sequence
from pioneer.base import S, A
from pioneer.policies.base import Policy
from pioneer.utils import Discrete


def create_uniform_policy(states: Sequence[S], actions: Sequence[A]) -> Policy[S, A]:
    return {s: Discrete.uniform(actions) for s in states}
