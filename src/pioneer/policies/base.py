from typing import Mapping, MutableMapping
from pioneer.utils.dist import Discrete
from pioneer.base import S, A

Policy = Mapping[S, Discrete[A]]
MutablePolicy = MutableMapping[S, Discrete[A]]
