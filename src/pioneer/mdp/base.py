from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, Iterator, Optional, Sequence

from pioneer.base import S, A, R
from pioneer.policies.base import Policy


@dataclass
class Step(Generic[S, A, R]):
    start: S
    end: S
    action: A
    reward: Optional[R]


class MDP(Generic[S, A, R], ABC):
    @abstractmethod
    def iterate(
        self, state: S, policy: Policy[S, A], action: Optional[A] = None
    ) -> Iterator[Step[S, A, R]]:
        pass

    def episode(
        self, state: S, policy: Policy[S, A], action: Optional[A] = None
    ) -> Iterator[Step[S, A, R]]:

        if self.terminal(state):
            return
        steps = self.iterate(state, policy, action)
        while True:
            try:
                step = next(steps)
            except StopIteration:
                break
            yield step
            if self.terminal(step.end) is None:
                raise RuntimeError("Not an episodic MDP")
            if self.terminal(step.end) is True:
                break

    @property
    @abstractmethod
    def states(self) -> Sequence[S]:
        pass

    @abstractmethod
    def actions(self, state: S) -> Sequence[A]:
        pass

    def terminal(self, state: S) -> Optional[bool]:
        # pylint: disable=unused-argument
        return None
