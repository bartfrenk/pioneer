from dataclasses import dataclass
from typing import Dict, Tuple, Generic, Sequence, Set, Mapping, Callable, Optional, Iterator
from pioneer.utils.dist import Discrete
from pioneer.base import S, A, R
from pioneer.mdp.base import MDP, Step
from pioneer.policies.base import Policy


@dataclass(init=False)
class Dynamics(Generic[S, A, R]):
    state: Mapping[Tuple[S, A], Discrete[S]]
    reward: Mapping[Tuple[S, A], Discrete[R]]

    def __init__(self, state, reward):
        self.state = state
        self.reward = reward
        self.__states, self.__actions = self._compute_states_and_actions()

    @property
    def states(self):
        return self.__states

    @property
    def actions(self):
        return self.__actions

    def _compute_states_and_actions(self) -> Tuple[Sequence[S], Mapping[S, Sequence[A]]]:
        states = set()
        actions: Dict[S, Set[A]] = {}
        for (state, action) in self.state:
            states.add(state)
            actions.setdefault(state, set()).add(action)
        for state in actions:
            actions[state] = list(actions[state])  # type: ignore
        return (list(states), actions)  # type: ignore


class KnownMDP(MDP[S, A, R]):
    _dynamics: Dynamics[S, A, R]

    def __init__(
        self, dynamics: Dynamics[S, A, R], terminal: Optional[Callable[[S], bool]] = None
    ):
        self._dynamics = dynamics
        self._terminal = terminal

    def iterate(
        self, state: S, policy: Policy[S, A], action: Optional[A] = None
    ) -> Iterator[Step[S, A, R]]:
        start = state
        a = action or policy[start].draw()
        while True:
            r = self._dynamics.reward[(start, a)].draw()
            end = self._dynamics.state[(start, a)].draw()
            yield Step(start=start, end=end, action=a, reward=r)
            start = end
            a = policy[start].draw()

    def actions(self, state: S) -> Sequence[A]:
        if state not in self._dynamics.actions:
            raise ValueError("Unknown state")
        return self._dynamics.actions[state]

    @property
    def states(self) -> Sequence[S]:
        return self._dynamics.states

    def terminal(self, state: S) -> Optional[bool]:
        if self._terminal is None:
            return None
        return self._terminal(state)
