from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Generic, List, Optional, Sequence, TypeVar

import numpy as np

X = TypeVar("X")


class Discrete(Generic[X], ABC):
    @abstractmethod
    def draw(self):
        pass

    @classmethod
    def deterministic(cls, x: X, xs: Optional[Sequence[X]] = None) -> Deterministic:
        return Deterministic(xs or [x], x)

    @classmethod
    def create(cls, xs: Sequence[X], ws: Sequence[float]) -> Probabilistic:
        return Probabilistic(xs, ws)

    @classmethod
    def uniform(cls, xs: Sequence[X]) -> Probabilistic:
        return cls.create(xs, [1 for _ in xs])


class Probabilistic(Discrete[X]):
    _xs: List[X]

    def __init__(self, xs: Sequence[X], weights: Sequence[float]):
        self._xs = list(xs)
        self._weights = list(weights)
        self._total = sum(self._weights)
        self._steps = np.cumsum(self._weights)

    def draw(self) -> X:
        i: int = np.argmax(np.random.uniform(high=self._total) < self._steps)  # type: ignore
        return self._xs[i]


class Deterministic(Discrete[X]):
    def __init__(self, xs: Sequence[X], x: X):
        self._xs = list(xs)
        self._x = x

    def draw(self) -> X:
        return self._x

    def __eq__(self, other):
        if not isinstance(other, Deterministic):
            return False
        return other._xs == self._xs and other._x == self._x
